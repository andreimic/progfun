package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c <= 0 || c >= r) 1
    else pascal(c-1, r-1) + pascal(c, r-1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    if (chars.length == 0) true
    else {
      def updateCount(count: Int, c: Char) = {
        if ('('.equals(c)) count + 1
        else
          if (')'.equals(c))
            if (count <= 0) count - 2
            else count -1
          else count
      }

      val unclosed = chars.foldLeft(0)(updateCount)
      unclosed == 0
    }
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0) 1
    else if (coins.length == 0 || money < 0) 0
    else countChange(money - coins.head, coins) + countChange(money, coins.tail)
  }
}
